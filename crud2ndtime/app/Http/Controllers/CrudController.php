<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class CrudController extends Controller
{
    // public function home(){
    // 	return view('pages.home');
    // }

    public function insert(){
    	return view('pages.insert');
    }

    public function postInsert(Request $request){
    	// return $request->all();
    	$inputUsers=new User();
    	$inputUsers->name=$request->input('name');
    	$inputUsers->email=$request->input('email');
    	$inputUsers->password=bcrypt($request->input('password'));

    	$inputUsers->save();
    	return redirect(action('CrudController@view'));

    }


    public function view(){
    	$allUser=User::paginate(4);
    	// return $allUser;
    	return view('pages.home', compact('allUser'));
    }


    public function update($id){
    	$editUser=User::findOrFail($id);
    	return view('pages.update', compact('editUser'));
    }


    public function postUpdate(Request $request, $id){
    	$editUser=User::findOrFail($id);
    	$editUser->name=$request->input('name');
    	$editUser->email=$request->input('email');

    	$editUser->save();
    	return redirect(action('CrudController@view'));
    }


    public function delete($id){
    	$deleteUser=User::findOrFail($id);
    	$deleteUser->delete();
    	return redirect(action('CrudController@view'));
    }

}
