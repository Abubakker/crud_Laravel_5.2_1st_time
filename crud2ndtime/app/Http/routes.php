<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'CrudController@view');
Route::get('/insert', 'CrudController@insert');


Route::post('postInsert', 'CrudController@postInsert');


Route::get('update/{id}', 'CrudController@update');

Route::post('postUpdate/{id}', 'CrudController@postUpdate');


Route::get('delete/{id}', 'CrudController@delete');



// Route::post('view', 'CrudController@view');