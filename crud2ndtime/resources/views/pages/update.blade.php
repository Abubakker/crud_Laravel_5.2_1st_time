@extends('pages.masterLayout')

@section('title', 'Update')



@section('content')
	<h1>This is Update page for CRUD</h1>
	<pre></pre>


	<form action="{{url('postUpdate', $editUser->id)}}" method="post">

	<!-- <form action="{{action('CrudController@postInsert')}}" method="post"> -->

	<input type="hidden" name="_token" value="{{ csrf_token() }}">



	  <div class="form-group">
	    <label for="name">User name</label>
	    <input type="text" name="name" class="form-control" id="name" value="{{$editUser->name}}">
	  </div>
	  <div class="form-group">
	    <label for="exampleInputEmail1">Email address</label>
	    <input type="email" name="email" class="form-control" id="exampleInputEmail1" value="{{$editUser->email}}">
	  </div>
	  <button type="submit" class="btn btn-info">Update</button>
	</form>
	
	
	
	
@endsection