@extends('pages.masterLayout')

@section('title', 'Insert')



@section('content')
	<h1>This is Insert page for CRUD</h1>
	<pre></pre>


	<form action="{{url('postInsert')}}" method="post">

	<!-- <form action="{{action('CrudController@postInsert')}}" method="post"> -->

	<input type="hidden" name="_token" value="{{ csrf_token() }}">



	  <div class="form-group">
	    <label for="name">User name</label>
	    <input type="text" name="name" class="form-control" id="name" placeholder="Md. Abu bakker siddique">
	  </div>
	  <div class="form-group">
	    <label for="exampleInputEmail1">Email address</label>
	    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="bakker311042@gmail.com">
	  </div>
	  <div class="form-group">
	    <label for="exampleInputPassword1">Password</label>
	    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
	  </div>
	  <button type="submit" class="btn btn-success">Submit</button>
	</form>
	
	
	
	
@endsection