@extends('master_layout')



@section('title', 'View')



@section('content')
	<h1>This is View page for CRUD</h1>


	<pre></pre>			

		<table class="table">
		    <thead>
		      <tr>
		      	<th>Serial</th>
		        <th>First Name</th>
		        <th>Last Name</th>
		        <th>Email</th>
		        <th class="text-center">Action</th>
		      </tr>
		    </thead>
		    <tbody>
		    	<?php $counter=1; ?>
		    	@foreach($allData as $sigleData)
		    		<tr>
		    			<td>{{$counter++}}</td>
			    		<td>{{$sigleData->first_name}}</td>
			    		<td>{{$sigleData['last_name']}}</td>
			    		<td>{{$sigleData->email}}</td>
			    		<td class="text-center">
			    			<a href="{{url('single_view', $sigleData->id)}}"><button class="btn btn-info">Single View</button></a>
			    			<a href="{{url('update', $sigleData->id)}}"><button class="btn btn-success">Update</button></a>
			    			<a href="{{url('deleteBD', $sigleData->id)}}"><button class="btn btn-danger">Update</button></a>
			    			
			    			<!-- <form action="{{url('deleteBD', $sigleData->id)}}" method="post">
			    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			    			<button type="submit" class="btn btn-danger">Delete</button></form>		 -->    				
			    		</td>
			    	
			    	</tr>
			    @endforeach
		    	

		    
		    </tbody>
		  </table>

		  {!! $allData->render() !!}

		  <a href="insert"><h1>Add user</h1></a> 

			
	
@endsection