@extends('master_layout')



@section('title', 'Update')



@section('content')

	<h1>Welcome for Update your details</h1>

	<form class="form-inline" action="{{url('postUpdateDB', $user->id)}}" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputfName">First Name</label>First Name: 
	    <input type="text" name="fname" value="{{$user->first_name}}" class="form-control" id="exampleInputfName" placeholder="First Name">
	  </div>
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputlName">Last Name</label>Last Name: 
	    <input type="text" name="lname" value="{{$user->last_name}}" class="form-control" id="exampleInputlName" placeholder="Last Name">
	  </div>
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputEmail3">Email address</label>Email: 
	    <input type="email" name="email" value="{{$user->email}}" class="form-control" id="exampleInputEmail3" placeholder="Email">
	  </div>
	  <button type="submit" class="btn btn-info">Update</button>
	</form>


@endsection