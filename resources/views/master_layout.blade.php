<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

	<div class="container-fluid">
	@section('manu')
		<h1>
			<a href="/">Home</a>
			<a href="insert">Insert</a>
			<a href="view">View</a>
			<a href="update">Update</a>
			<a href="delete">Delete</a>
		</h1>
	@show
	</div>
	

	<div class="container">
		@yield('content')
	</div>
	
</body>
</html>