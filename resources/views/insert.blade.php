@extends('master_layout')



@section('title', 'Insert')



@section('content')

	<h1>Welcome for Insert your details</h1>

	<form class="form-inline" action="{{url('postInsert')}}" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputfName">First Name</label>First Name: 
	    <input type="text" name="fname" class="form-control" id="exampleInputfName" placeholder="First Name">
	  </div>
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputlName">Last Name</label>Last Name: 
	    <input type="text" name="lname" class="form-control" id="exampleInputlName" placeholder="Last Name">
	  </div>
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputEmail3">Email address</label>Email: 
	    <input type="email" name="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
	  </div>
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputPassword3">Password</label>Password:
	    <input type="password" name="password" class="form-control" id="exampleInputPassword3" placeholder="Password">
	  </div>
	  <button type="submit" class="btn btn-success">Save</button>
	</form>


@endsection