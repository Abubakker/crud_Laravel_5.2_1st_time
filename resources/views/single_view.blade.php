@extends('master_layout')



@section('title', 'Single view')



@section('content')
	<h1>This is Single View page for CRUD</h1>


	<pre></pre>			

		<table class="table">
		    <thead>
		      <tr>
		      	<th>User ID</th>
		        <th>First Name</th>
		        <th>Last Name</th>
		        <th>Email</th>
		        <th>Created Date and Time</th>
		        <th>Updated Date and Time</th>
		      </tr>
		    </thead>
		    <tbody>
		    	<td>{{$user->id}}</td>
		    	<td>{{$user->first_name}}</td>
		    	<td>{{$user->last_name}}</td>
		    	<td>{{$user->email}}</td>
		    	<td>{{$user->created_at}}</td>
		    	<td>{{$user->updated_at}}</td>
		    </tbody>
		  </table> 
		  <a href="/view"><h1>View All</h1></a>

			
	
@endsection