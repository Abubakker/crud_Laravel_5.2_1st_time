<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});



Route::get('/', 'CrudController@home');
Route::get('/insert', 'CrudController@insert');
Route::post('postInsert', 'CrudController@postInsert');

Route::get('/view', 'CrudController@viewDB');
Route::get('/single_view/{idview}', 'CrudController@single_view');


Route::get('/update/{idview}', 'CrudController@update');

Route::post('/postUpdateDB/{id}', 'CrudController@postUpdateDB');

Route::get('/deleteBD/{id}', 'CrudController@deleteBD');