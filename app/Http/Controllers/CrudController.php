<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class CrudController extends Controller
{
    public function home(){
    	return view('home');
    }

    public function insert(){
    	return view('insert');
    }

    public function postInsert(Request $request){
    	/*$allValue=$request->all();
    	return $allValue;*/   
    	// return $request->all(); 2tai same
    	$insert= new User();
    	$insert->first_name=$request->input('fname');
    	$insert->last_name=$request->input('lname');
    	$insert->email=$request->input('email');
    	$insert->password=bcrypt($request->input('password'));

    	$insert->save();
    	return redirect(action('CrudController@viewDB'));
    }



    public function viewDB(){
    	$allData=User::paginate(5);
    	// return $allData;
    	return view('view', compact('allData'));    	
    }

	

    public function single_view($id){
    	$user=User::findOrFail($id);
    //	return $user;
    	return view('single_view', compact('user'));
    }

    public function update($id){
    	$user=User::findOrFail($id);
    //	return $user;
    	return view('update', compact('user'));
    }


    public function postUpdateDB(Request $request, $id){
    	
    	//$update_user=new User();

    	$user=User::findOrFail($id);

    	$user->first_name=$request->input('fname');
    	$user->last_name=$request->input('lname');
    	$user->email=$request->input('email');

    	$user->save();
    	return redirect(action('CrudController@viewDB'));
    }

    public function deleteBD($id){
    	$user=User::findOrFail($id);

    	$user->delete();
    	return redirect(action('CrudController@viewDB'));
    }



}
